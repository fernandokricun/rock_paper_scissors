import ast
import os
import sys
import types
from variables import PATH, PHASE, RESULTS, VALUES, ASCII
from importlib.machinery import SourceFileLoader
from random import shuffle
from time import sleep


def clear():
    """
    Clear Screen
    """
    if os.name == 'posix':
        _ = os.system('clear')
    else:
        _ = os.system('cls')


def get_player(path):
    """
    Get player's choice and name from a file

    :param path: Path of the file
    :return: Player's choice ('R', 'P' or 'C') and the author's name
    """
    with open(path) as f:
        p = ast.parse(f.read())

    for node in p.body[:]:
        if not isinstance(node, (ast.FunctionDef, ast.Import, ast.ImportFrom)):
            p.body.remove(node)

    module = types.ModuleType("mod")
    code = compile(p, "mod.py", 'exec')
    sys.modules["mod"] = module
    exec(code,  module.__dict__)

    import mod

    return mod.jugar()


def handle_except(file, except_type=None):
    """
    Handle exceptions

    :param file: Name of the file
    :param except_type: Exception type
    :return: Exit the program
    """
    clear()
    print("ERROR")
    print('-------')
    print(f'Revisar {file}\n')

    if except_type is AttributeError:
        print('La funcion debe llamarse jugar()')
    elif except_type is ValueError:
        print('La funcion debe devolver:')
        print('    1) La opcion jugada (R, P, C, L, S)')
        print('    2) El nombre de la persona')
        print('Ejemplo:')
        print('    return "R", "Peter Parker"')
    elif except_type is KeyError:
        print('Las jugadas pueden ser unicamente R, P, C, L o S')
    else:
        print('La funcion no anda')

    return quit()


# Only execute if it's the main program
if __name__ == '__main__':
    phase = 1
    clear()
    players = next(os.walk(PATH))[2]  # Get all the files from PATH

    shuffle(players)

    # Just for UX
    print('\n'.join(ASCII['HOMESCREEN']))
    sleep(1)
    input('\n\n   Presione enter para comenzar ')
    clear()
    print('\n'.join(ASCII['LOADING_FILES']))
    sleep(2)
    clear()

    # Finish when there is only one winner
    while len(players) > 1:

        # Use a bye if the number of players is odd
        if len(players) % 2 != 0:
            players.append('bye/bye.py')

        # Create the matches by joining the players in
        # groups of 2
        matches = [(players[i], players[i+1])
                   for i in range(0, len(players), 2)]

        try:
            current_phase = PHASE[len(players) // 2 - 1]
        except IndexError:
            current_phase = f'Fase {phase}'

        players = []  # Now players will be the winners of the current phase
        draw = True
        tiebreak = False

        # Keep playing until there are no draws
        while draw:
            draw = False
            results = []

            print(f'{current_phase} -'
                  f' {"Desempate" if tiebreak else "Partidas"}')
            print('--------------------------')

            # Play the matches
            for match in matches:

                # Get the choices and names of the players
                try:
                    play1, player1 = get_player(f'{PATH}/{match[0]}')
                except AttributeError as e:
                    handle_except(match[0], type(e))
                except ValueError as e:
                    handle_except(match[0], type(e))
                except Exception:
                    handle_except(match[0])

                try:
                    play2, player2 = get_player(f'{PATH}/{match[1]}')
                except AttributeError as e:
                    handle_except(match[1], type(e))
                except ValueError as e:
                    handle_except(match[1], type(e))
                except Exception:
                    handle_except(match[1])

                print(f'\n{player1}\n vs\n{player2}')

                try:
                    result = RESULTS[VALUES[play1][0]][VALUES[play2][0]]
                except KeyError as e:
                    handle_except(f'{match[0]} o {match[1]}', type(e))

                results.append([f'\n{player1} jugo {VALUES[play1][1]}'
                                f' y {player2} jugo {VALUES[play2][1]}.',
                                result])

                if result < 0:
                    results[-1][0] += f'\n Empate, juegan de nuevo.'
                else:
                    results[-1][0] += (f'\n {(player1, player2)[result]}'
                                       ' gano')
                sleep(0.5)

            input('\nPresione enter para continuar ')

            clear()
            print('\n'.join(ASCII['PLAYING']))
            sleep(2)
            clear()

            print(f'{current_phase}{" (desempate)" if tiebreak else ""}'
                  f' - Resultados')
            print('--------------------------')
            matches_copy = list(matches)
            matches = []  # Now matches will be the remaining draws
            for result, match in zip(results, matches_copy):
                sleep(1)
                print(result[0])
                if result[1] < 0:
                    matches.append(match)
                    draw = True
                    tiebreak = True
                else:
                    players.append(match[result[1]])  # Append the winners

            input('\nPresione enter para continuar ')
            clear()

        phase += 1
    clear()
