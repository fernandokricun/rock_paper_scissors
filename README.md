# Competencia Piedra Papel o Tijera (Lizard Spock)

Proyecto creado unicamente utilizando modulos de la libreria estandar

## Requisitos

En la carpeta [programas](programas/) se deben agregar a todos los programas participantes en la competicion. 

La funcion de cada programa debera tener la siguiente estructura:
```Python
def jugar():
    # (...)
    return choice, name
```

**NO PONER PRINTS NI INPUTS ADENTRO DE LA FUNCION**

## Opciones para elegir
|Letra|Nombre Completo|
|-----|---------------|
|  R  |    Piedra     |
|  P  |    Papel      |
|  C  |    Tijera     |
|  L  |    Lizard     |
|  S  |    Spock      |

