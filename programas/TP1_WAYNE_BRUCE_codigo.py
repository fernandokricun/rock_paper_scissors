import random


def jugar():
    return random.choice('RPCLS'), 'Bruce Wayne'


valores = {
    'R': (0, 'Piedra'),
    'P': (1, 'Papel'),
    'C': (2, 'Tijera'),
    'L': (3, 'Lizard'),
    'S': (4, 'Spock')
}

resultados = (
    # R  P   C   L   S
    (0, -1,  1,  1, -1),  # Rock
    (1,  0, -1, -1,  1),  # Paper
    (-1, 1,  0,  1, -1),  # Scissors
    (-1, 1, -1,  0,  1),  # Lizard
    (1, -1,  1, -1,  0),  # Spock
)

resultado_palabra = ('Empataste', 'Ganaste', 'Perdiste')

if __name__ == "__main__":
    # The game begins
    jugador = input('Piedra [R], Papel [P], Tijera [C],'
                    ' Lizard [L], Spock [S]? ').upper()

    while (jugador != 'R' and jugador != 'P' and jugador != 'C'
            and jugador != 'L' and jugador != 'S'):
        print('Elegi una respuesta valida')
        jugador = input('Piedra [R], Papel [P] o Tijera [C]? ').upper()

    computadora = jugar()[0]

    resultado = resultados[valores[jugador][0]][valores[computadora][0]]

    print(f'Vos elegiste {valores[jugador][1]}'
          f' y yo elegi {valores[computadora][1]}.'
          f' {resultado_palabra[resultado]}.')
