import random


def jugar():
    return random.choice('RPCLS'), 'Tony Stark'


valores = {
    'R': (0, 'Piedra'),
    'P': (1, 'Papel'),
    'C': (2, 'Tijera'),
    'L': (3, 'Lizard'),
    'S': (4, 'Spock')
}

resultados = (
    #  Rock          Paper      Scissors      Lizard       Spock
    ('Empataste', 'Perdiste',  'Ganaste',   'Ganaste',   'Perdiste'),   # Rock
    ('Ganaste',   'Empataste', 'Perdiste',  'Perdiste',  'Ganaste'),    # Paper
    ('Perdiste',  'Ganaste',   'Empataste', 'Ganaste',   'Perdiste'),   # Scissors
    ('Perdiste',  'Ganaste',   'Perdiste',  'Empataste', 'Ganaste'),    # Lizard
    ('Ganaste',   'Perdiste',  'Ganaste',   'Perdiste',  'Empataste'),  # Spock
)


if __name__ == "__main__":
    # Empieza el juego
    jugador = input('Piedra [R], Papel [P] o Tijera [C]? ').upper()

    while jugador != 'R' and jugador != 'P' and jugador != 'C':
        print('Elegi una respuesta valida')
        jugador = input('Piedra [R], Papel [P] o Tijera [C]? ').upper()

    computadora = jugar()[0]

    print(f'Vos elegiste {valores[jugador][1]}'
          f' y yo elegi {valores[computadora][1]}.'
          f' {resultados[valores[jugador][0]][valores[computadora][0]]}.')
